## Pharmacy2020 Assignment 

Login system with Laravel, ReactJS and MySQL.

### Installation

- Run `npm i` within `client` folder
- Run `composer install` within root
- Double check `.env` file for proper mysql credentials 
- Serve backend using `php artisan serve`
- Serve client using `npm install`

### Author
Candidate: George Zafiris
License: MIT